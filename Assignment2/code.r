simulate.round <- function(n.doors, policy){
  
  correct.door <- sample(1: n.doors, 1)
  player.door <- sample(1:n.doors, 1)
  
  if(player.door==correct.door){
    
    if(policy=="KEEP"){
      return(TRUE);
    }
    else if(policy=="CHANGE"){
      return(FALSE);
    }
  }
  
  else{
    
    if(policy=="KEEP"){
      return(FALSE);
    }
    else if(policy=="CHANGE"){
      return(TRUE);
    }
 }
  
}

main <- function(policy, n.min, n.max, n.trials){
  win.probability <- rep(0, n.max)
  for(n.doors in n.min:n.max){
    win <- 0
    for(i in 1:n.trials){
      win <- win + if(simulate.round(n.doors, policy)){
        1
      } else {
        0
      }
    }
    win.probability[n.doors] <- win/n.trials
  }
  plot(3:n.max, win.probability[3:n.max], type="l", xlab="# of Doors", ylab="Win Probability", main=policy)
}

main("KEEP", 3, 100, 10*1000)