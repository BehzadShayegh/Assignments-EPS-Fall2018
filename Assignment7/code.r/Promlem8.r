library(dplyr)

gr96 = eps_96_grades_cleaned
gr96 <- select(gr96, Assignment..Final.Exam..Real.);
gr96 <- filter(gr96, gr96$Assignment..Final.Exam..Real. != '-')
gr96$total <- gr96$Assignment..Final.Exam..Real.;

data <- as.numeric(as.character(gr96$total));
m1 = mean(data)
m2 = mean(data ^ 2)
alpha = (m1 * m1) / (m2 - m1 * m1)
teta = (m2 - m1 * m1) / m1
xfit = seq(from = min(data), to = max(data), length = length(data));
sg = dgamma(xfit, shape = alpha, scale = teta);
hist(data, freq=FALSE)
lines(xfit, sg, col = "Red");


n = 15
s.data <- as.numeric(as.character(sample_n(gr96, n)$total));
print(s.data)
miu = 24
sm = mean(s.data)
ssd = sd(s.data)
error_range = 1.96 * ssd / sqrt(n);
if ( abs(miu-sm) < error_range ){
  print("True");
}else{
  print("False");
}
