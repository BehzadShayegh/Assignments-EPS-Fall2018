gr97 = eps_97_grades_cleaned

gr97 <- gr97[!is.na(gr97$Assignment..Quiz.1..Real.) , ]

gr97 <- gr97[!is.na(gr97$Assignment..Quiz.2..Real.) , ]

gr97$quiz.sum = gr97$Assignment..Quiz.1..Real. + gr97$Assignment..Quiz.2..Real.

real_mean = 0;
for(t in 1:dim(gr97)[1])
  real_mean = real_mean + gr97[t, "quiz.sum"]
real_mean = real_mean/dim(gr97)[1]

N = c(10,20,40);
answer = c(0,0,0)

turn = 0

for(n in N) {
  x = array( 0, dim=c(n))
  turn = turn + 1
  in_range = 0
  
  for(i in 1:100000) {
    
    error_range = 0
    sum = 0
    
    for(t in 1:n) {
      row_number = floor(runif(1, min=1, max=dim(gr97)[1]))
      x[t] = gr97[row_number, "quiz.sum"]
      sum = sum + x[[t]]
    }
    Xbar = sum/n
    
    S2 = 0;
    for(t in 1:n)
      S2 = S2 + ((x[[t]]-Xbar)^2)/(n-1)
    S = sqrt(S2)
    
    error_range = S*1.96/sqrt(n)
    if( abs(real_mean - Xbar) < error_range )
      in_range = in_range + 1
  }
  
  print(in_range)
  turn = turn + 1
  answer[turn] = in_range 
}