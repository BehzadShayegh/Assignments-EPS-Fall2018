library(animation);
library(plotrix);


line.length <- function(points){
  return(norm(points[1,]-points[2,], type="2"));
}


draw.chord <- function(points, threshold){
  color <- "blue";
  chord.length <- line.length(points);
  longer <- chord.length > threshold;
  if(longer){
    color <- "red";
  }
  lines(points[,1], points[,2], col=color);
  
  return(longer);
}


draw.chord.by.midpoint <- function(center.x, center.y, radius,
                                   midpoint.x, midpoint.y){
  
  side.length <- sin(pi/3)*radius*2;
  
  len <- sqrt((midpoint.x-center.x)^2+(midpoint.y-center.y)^2);
  theta <- atan2(midpoint.y-center.y, midpoint.x-center.x);
  
  chord.halflength = if(radius^2 >= len^2) {sqrt(radius^2-len^2)}
                      else {sqrt(len^2-radius^2)};
  theta1 <- theta + pi/2;
  theta2 <- theta1 + pi;
  
  chord.endpoint1.x <- midpoint.x + cos(theta1)*chord.halflength;
  chord.endpoint1.y <- midpoint.y + sin(theta1)*chord.halflength;
  chord.endpoint2.x <- midpoint.x + cos(theta2)*chord.halflength;
  chord.endpoint2.y <- midpoint.y + sin(theta2)*chord.halflength;
  
  points <- matrix(c(chord.endpoint1.x, chord.endpoint2.x,
                     chord.endpoint1.y, chord.endpoint2.y), nrow=2, ncol=2)
  
  return(draw.chord(points, side.length));
}


method.one <- function(center.x, center.y, radius){
  T <- runif(1, 0, 2*pi)
  x1 <- center.x+radius*cos(T)
  y1 <- center.y+radius*sin(T)
  
  T <- runif(1, 0, 2*pi)
  x2 <- center.x+radius*cos(T)
  y2 <- center.y+radius*sin(T)
  
  x_ = (x1+x2)/2
  y_ = (y1+y2)/2
  
  return(c(x_ , y_));
}


method.two <- function(center.x, center.y, radius){
  T <- runif(1, 0, 2*pi)
  R <- runif(1, 0, radius)
  
  x_ <- center.x+R*cos(T)
  y_ <- center.y+R*sin(T)
  
  return(c(x_ , y_));
}


method.three <- function(center.x, center.y, radius){
  repeat{
    x_ <- runif(1, center.x-radius, center.x+radius)
    y_ <- runif(1, center.y-radius, center.y+radius)
    if( (center.x-x_)^2 + (center.y-y_)^2 < radius^2 ){
      break
    }
  }  
  
  return(c(x_ , y_));
}


main <- function(){
  n.trials <- 200;
  xrange <- 49;
  yrange <- 49;
  center.x <- 25;
  center.y <- 25;
  radius <- 24;
  hold.frames <- 25;
  
  generate.midpoints <- function(method){
    midpoints <- vector(mode="list", length=n.trials);
    
    for(iteration in 1:n.trials){
      midpoints[[iteration]] <- method(center.x, center.y, radius);
    }
    
    return(midpoints);
  }
  
  draw.frame <- function(midpoints, n.chords){
    longer <- 0;
    total <- 0;
    plot(1:xrange, 1:yrange, type="n", xlab="", ylab="", main="Bertrand", asp=1);
    draw.circle(center.x, center.y, radius);
    for(chord.idx in 1:n.chords){
      total <- total + 1;
      midpoint <- midpoints[[chord.idx]];
      is.longer <- draw.chord.by.midpoint(center.x, center.y, radius,
                                           midpoint[1], midpoint[2]);
      if(is.longer){
        longer <- longer + 1;
      }
    }
    string <- sprintf("Total: %d, Longer: %d, Probability: %f",
                      total, longer, longer/total);
    mtext(string);
  }
  
  ani.options(interval=0.1);
  ms <- c(method.one, method.two, method.three);
  saveHTML({
    for(method in ms){
      midpoints <- generate.midpoints(method);
      for(iteration in 1:n.trials){
        draw.frame(midpoints, iteration);
      }
      for(i in 1:hold.frames){
        draw.frame(midpoints, n.trials);
      }
    }
  });
}


main();
