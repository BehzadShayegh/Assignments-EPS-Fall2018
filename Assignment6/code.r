n <- c(1, 2, 5, 10, 100, 1000, 10000);

for(i in n)
{
    Sn = 0;
    
    for(j in 1:i)
    {
        Sn = Sn + rexp(10000, 3);
    }
    
    Sn = Sn / i;

    
    
    hist(Sn, breaks = 10, freq=FALSE, main=paste(as.character(i)));
    xrange <- seq(min(Sn), max(Sn), length = 100);
    
    y_norm_fit <- dnorm(xrange, mean = mean(Sn), sd = sd(Sn));
    y_exp_fit <- dexp(xrange, 3);
    
    lines(xrange, y_norm_fit, col = "blue");
    lines(xrange, y_exp_fit, col = "green");
    
    Sys.sleep(1)

}